import csv
from itertools import groupby

"""


https://www150.statcan.gc.ca/t1/tbl1/en/cv!recreate.action?pid=3610057801&selectedNodeIds=1D1,2D36,2D1,2D35,2D3,2D6,2D29,2D30,2D34,2D29All,3D11,3D1&checkedLevels=&refPeriods=19900701,20180701&dimensionLayouts=layout2,layout2,layout3,layout2&vectorDisplay=false



https://docs.google.com/spreadsheets/d/17yUgIBjDMcai4DbE8ZLTpxsXxnXSRiE3_30ncuLYnUo/edit#gid=2017546623

"""
def go(file):

    def read(file):
        with open(file) as file:
            tsvin = csv.reader(file, delimiter=',', doublequote=True)
            c = 0

            for row in tsvin:

                c += 1

                # skip first and empty rows
                if c == 1 or len(row) < 1:
                    continue

                category = row[4]

                if not category.startswith("Net lending"):
                    continue

                quarter = row[0]
                sector = row[3]
                n = int(row[11])

                yield quarter, sector, n

    sectors_prefixes = ["Hous", "Corp", "Fed", "Non-res", "Stat", "Social", "Other levels"]

    def quarterly():

        def get_quarter_col(t):
            return t[0]

        for quarter, rows in groupby(sorted(read(file), key=get_quarter_col), get_quarter_col):

            res = []
            rows = list(rows)
            for sector in sectors_prefixes:
                for row in rows:
                    if row[1].startswith(sector):
                        res.append(row[2])

            yield [quarter] + res

    def sum_col(rows, i):
        return sum(map(lambda r: r[i], rows))

    for year, vals in groupby(quarterly(), lambda r: r[0].split("-")[0]):

        vals = list(vals)
        house = sum_col(vals, 1)
        corp = sum_col(vals, 2)
        fed = sum_col(vals, 3)
        foreign = sum_col(vals, 4)
        discr = sum_col(vals, 5)
        social = sum_col(vals, 6)
        other_levels = sum_col(vals, 7)

        net = house + corp + fed + foreign + social + other_levels + discr

        # we distribute the statistical discrepancy on 3 sectors
        discr_adj = round(discr / 3.0)

        out_row = [
            year,
            fed + other_levels + discr_adj,     # gov sector
            house + social + corp + discr_adj,  # households, corps, social security funds
            foreign + discr_adj,                # foreign
            net                                 # net
        ]

        print("\t".join(map(str, out_row)))


if __name__ == '__main__':
    go("3610057801_databaseLoadingData5.csv")